import flask

app = flask.Flask(__name__)

@app.route("/")
def ola():
    mensagem = "Programação de Redes com Python!"
    return flask.render_template("index.html", mensagem=mensagem)

@app.route("/upper", methods=['POST'])
def upper():
    json = flask.request.get_json()
    mensagem = json['message'].upper()
    return { "message": mensagem } 


app.run(host="0.0.0.0", port=80)